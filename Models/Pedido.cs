public class Pedido
{
    private decimal total = 0;
    public int PedidoId { get; set; }
    public DateTime Data { get; set; }
    public decimal Total 
    {
        get
        {
            return Itens.Sum(item => item.Total);
        }
    }
    // public decimal Total 
    // {
    //     get{
    //         decimal total = 0;
    //         foreach (var item in Itens)
    //         {
    //             total += item.Total;
    //         }
    //         return total;
    //     }
    // }
        
    public Cliente Cliente { get; set; }
    public List<Item> Itens { get; set; }

      
}

